package com.example.loginusingapi

data class LoginResponse (val error: Boolean, val message:String, val user: User,val status:String)
