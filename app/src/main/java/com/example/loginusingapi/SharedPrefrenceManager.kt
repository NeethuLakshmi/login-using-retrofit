package com.example.loginusingapi

import android.content.Context

class SharedPrefrenceManager private constructor(private val mCtx:Context){
    val isLoggin:Boolean
    get(){

        val sharedPrefrences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        return sharedPrefrences.getInt("id",-1) != -1
    }
    val user:User
    get (){
        val sharedPrefrences= mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE)
        return User(
            sharedPrefrences.getString("email",null).toString(),
       sharedPrefrences.getString("password",null).toString(),
            sharedPrefrences.getInt("user type ",1)
        )
    }
    fun saveUser(user: User){
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("email",user.email_id)
      //  editor.putString("password",user.password)
        editor.apply()
    }
    fun clear (){
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE)
        val editor =sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }
    companion object{
        val SHARED_PREF_NAME = "sharedpreff_name"
        private  var mInstance:SharedPrefrenceManager?= null
        @Synchronized
        fun getInstance(mCtx: Context):SharedPrefrenceManager {
            if (mInstance == null){
                mInstance = SharedPrefrenceManager(mCtx)}
            return mInstance as SharedPrefrenceManager
            }

        }
    }
