package com.example.loginusingapi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    val userType:Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loginbutton.setOnClickListener()
        {
            val email = logineditText.text.toString()
            val password =logineditTextPassword.text.toString()
            if (email.isEmpty() && password.isEmpty()) {
                logineditText.requestFocus()
                logineditText.error = "Email id Requierd"
                logineditTextPassword.requestFocus()
                logineditTextPassword.error = "Password Requierd"
            }
            RetrofitClient.instance.login(email, password,userType).enqueue(object :
                Callback<LoginResponse>{

                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    if (response.body()?.status=="success"){
                        Toast.makeText(applicationContext,"success", Toast.LENGTH_LONG).show()
                       SharedPrefrenceManager.getInstance(applicationContext).saveUser(response.body()?.user!!)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        val intent = Intent (applicationContext,MainActivity2::class.java)
                        startActivity(intent)
                    }
                    else if (response.body()?.status=="error"){
                        Toast.makeText(applicationContext,"Failed", Toast.LENGTH_LONG).show()}
                    else{
                        Toast.makeText(applicationContext, "failure", Toast.LENGTH_LONG).show()
                    }

                }  override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, "failure1", Toast.LENGTH_LONG).show()
                }
            }
            )

        }
    }
    override fun onStart() {
        super.onStart()
        if (SharedPrefrenceManager.getInstance(this).isLoggin) {
            val intent = Intent(applicationContext, MainActivity2::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            startActivity(intent)

        }
    }
}
